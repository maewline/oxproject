package com.mycompany.oxproject;

import java.util.Scanner;

/**
 *
 * @author Arthi
 */
public class OX {

    static char turn = 'X';
    static String checkWin = "Draw";
    static int endGame = 0;

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String[][] arr = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        System.out.println("Welcome to OX Game");

        while (endGame == 0) {
            if (CheckCondition(arr)) {
                break;
            }
            if (turn == 'X') {
                PrintTable(arr);
                ShowTextTurnX();
                int x = kb.nextInt();
                int y = kb.nextInt();
                EditXTable(x, y, arr);

            } else {
                PrintTable(arr);
                ShowTextTurnO();
                int x = kb.nextInt();
                int y = kb.nextInt();
                EditTableO(x, y, arr);
            }
        }
        WinnerCheck();
    }

    private static boolean CheckCondition(String[][] arr) {
        if (CheckHorizontal(arr)) {
            endGame = 1;
            return true;
        } else {
            if (CheckVertical(arr)) {
                endGame = 1;
                return true;
            } else if (CheckLeftDiagonal(arr)) {
                endGame = 1;
                return true;
            } else if (CheckRightDiagonal(arr)) {
                endGame = 1;
                return true;
            } else if (CheckDraw(arr) == 9) {
                endGame = 1;
                PrintTable(arr);
                return true;
            }
        }
        return false;
    }

    private static void ShowTextTurnO() {
        System.out.println("turn O");
        System.out.println("Please input row, col: ");
    }

    private static void ShowTextTurnX() {
        System.out.println("turn X");
        System.out.println("Please input row, col: ");
    }

    private static void WinnerCheck() {
        if (checkWin.equals("X")) {
            System.out.println(">>>X Win<<<");
        } else if (checkWin.equals("O")) {
            System.out.println(">>>O Win<<<");
        } else {
            System.out.println(">>>Draw<<<");
        }
    }

    private static int CheckDraw(String[][] arr) {
        int count = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (!arr[i][j].equals("-")) {
                    count++;
                }
            }
        }
        return count;
    }

    private static boolean CheckRightDiagonal(String[][] arr) {
        if (arr[0][2].equals(arr[1][1]) && arr[1][1].equals(arr[2][0]) && !arr[0][2].equals("-")) {
            checkWin = arr[0][2];
            PrintTable(arr);
            return true;
        }
        return false;
    }

    private static boolean CheckLeftDiagonal(String[][] arr) {
        if (arr[0][0].equals(arr[1][1]) && arr[1][1].equals(arr[2][2]) && !arr[1][1].equals("-")) {
            checkWin = arr[0][0];
            PrintTable(arr);
            return true;
        }
        return false;
    }

    private static boolean CheckVertical(String[][] arr) {
        for (int i = 0; i < 3; i++) {
            if (arr[0][i].equals(arr[1][i]) && arr[1][i].equals(arr[2][i]) && !arr[0][i].equals("-")) {
                checkWin = arr[0][i];
                PrintTable(arr);
                return true;
            }
        }
        return false;
    }

    private static boolean CheckHorizontal(String[][] arr) {
        for (int i = 0; i < 3; i++) {
            if (arr[i][0].equals(arr[i][1]) && arr[i][1].equals(arr[i][2]) && !arr[i][0].equals("-")) {
                checkWin = arr[i][0];
                PrintTable(arr);
                return true;
            }
        }
        return false;
    }

    private static void EditTableO(int x, int y, String[][] arr) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (x - 1 == i && y - 1 == j && arr[i][j].equals("-")) {
                    arr[i][j] = "O";
                    turn = 'X';
                    break;
                }
            }
        }
    }

    private static void EditXTable(int x, int y, String[][] arr) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (x - 1 == i && y - 1 == j && arr[i][j].equals("-")) {
                    arr[i][j] = "X";
                    turn = 'O';
                    break;
                }
            }
        }
    }

    private static void PrintTable(String[][] arr) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println(" ");
        }
    }
}
